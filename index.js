const counter = document.querySelector(".counter-number");
async function updateCounter(){
    let response = await fetch("https://7qx3kymaxp5gu7f3v4rozwd5ue0zbqag.lambda-url.ap-southeast-2.on.aws/");
    let data = await response.json();
    counter.innerHTML = ` Welcome, You are visitor number: ${data}`;
}

updateCounter();
//Scrolling Effects
$(document).ready(function(){
    $('a[href*="#"]').not('[href="#"]').not('[href="#0"]').click(function(event) {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) {
                        return false;
                    } else {
                        $target.attr('tabindex','-1');
                        $target.focus();
                    }
                });
            }
        }
    });
});

// Typing Effects
document.addEventListener("DOMContentLoaded", function() {
    const typingText = document.querySelector('.typing-effect');

    function restartAnimation() {
        typingText.style.animation = 'none';
        // Trigger reflow
        typingText.offsetHeight; 
        // Wait for 1 second before restarting the animation
        setTimeout(function() {
            typingText.style.animation = null;
        }, 1000); // 1000 milliseconds = 1 second
    }

    typingText.addEventListener('animationend', restartAnimation);
});



# Cloud Resume Challenge

![CI/CD Pipeline](https://img.shields.io/badge/pipeline-active-green) ![AWS Services](https://img.shields.io/badge/AWS-Services-orange) ![HTML5](https://img.shields.io/badge/html5-%23E34F26.svg?&style=for-the-badge&logo=html5&logoColor=white) ![CSS3](https://img.shields.io/badge/css3-%231572B6.svg?&style=for-the-badge&logo=css3&logoColor=white) ![JavaScript](https://img.shields.io/badge/javascript-%23323330.svg?&style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)

## Introduction
Welcome to my Cloud Resume Challenge project. This repository showcases my journey and expertise in Cloud Computing and DevOps Engineering. It is a testament to my skills in AWS, CI/CD practices, network security, and more. The project is hosted on AWS and features an automated CI/CD pipeline with a manual deployment option.

**Live Resume**: [My Cloud Resume](https://resume.hnguyen-cloud.com/)

## Project Development Stages
Explore different branches of this project to view each stage of my development journey.

## Project Highlights
- **Projects Showcase**: Detailed insights into various projects demonstrating my capabilities in cloud computing and DevOps.
- **Skills and Abilities**: An overview of my technical proficiencies in diverse technologies and methodologies.
- **Contact Information**: Available for collaborations and professional inquiries.

## Features
- **Responsive Design**: Ensures compatibility across various devices and screen sizes.
- **Professional Layout**: A clean and intuitive design for an optimal user experience.
- **Interactive Elements**: Showcasing my professional journey and technical skillset.

## Technologies Utilized
- **Frontend**: HTML5, CSS3, JavaScript - for a responsive and interactive web experience.
- **Cloud Services**: AWS (CloudFront, Lambda, DynamoDB, S3, Route 53) - for hosting, storage, and functionality.
- **CI/CD**: GitLab CI/CD pipeline for linting and automated deployment.

## Getting Started
To explore this project locally:
1. Clone the repository: `git clone [repo-url]`
2. Navigate to the project directory: `cd [project-name]`
3. Open `index.html` in a web browser.

## Contributions and Feedback
Your feedback and contributions are welcome! Feel free to open an issue or submit a pull request for improvements.

## Connect with Me
I'm open to collaborations and professional opportunities. Connect with me:

- **LinkedIn**: [Hoang T. Nguyen](https://www.linkedin.com/in/hoang-t-nguyen-a871541a4/)
- **Email**: [harrytnguyeen@gmail.com](mailto:harrytnguyeen@gmail.com)

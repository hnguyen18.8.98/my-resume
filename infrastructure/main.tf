// S3
    resource "aws_s3_bucket" "my_bucket" {
        bucket = "hnguyen-cloud-resume-challenge"
        tags = {
            project = "Cloud Resume Challenge"
        }
    }
    resource "aws_s3_bucket_policy" "cloud-resume_s3_bucket_allow_cloudfront" {
    bucket = aws_s3_bucket.my_bucket.id
    policy = jsonencode({
        Version = "2008-10-17"
        Id      = "PolicyForCloudFrontPrivateContent"
        Statement = [
        {
            Sid       = "AllowCloudFrontServicePrincipal"
            Effect    = "Allow"
            Principal = {
            Service = "cloudfront.amazonaws.com"
            }
            Action   = "s3:GetObject"
            Resource = "arn:aws:s3:::hnguyen-cloud-resume-challenge/*"
            Condition = {
            StringEquals = {
                "AWS:SourceArn" = "arn:aws:cloudfront::464216678525:distribution/E1W6KSKCIDXQ41"
            }
            }
        }
        ]
    })
    }

// CloudFront
    resource "aws_cloudfront_distribution" "my_distribution" {
        enabled             = true
        is_ipv6_enabled     = true
        http_version        = "http2"
        price_class         = "PriceClass_All"
        default_root_object = "index.html"
        retain_on_delete    = false
        wait_for_deployment = true

        aliases = ["resume.hnguyen-cloud.com"]

        default_cache_behavior {
            allowed_methods  = ["GET", "HEAD"]
            cached_methods   = ["GET", "HEAD"]
            target_origin_id = "hnguyen-cloud-resume-challenge.s3.ap-southeast-2.amazonaws.com"
            compress         = true
            viewer_protocol_policy = "https-only"
            cache_policy_id = "658327ea-f89d-4fab-a63d-7e88639e58f6"
        }

        origin {
            domain_name         = "hnguyen-cloud-resume-challenge.s3.ap-southeast-2.amazonaws.com"
            origin_id           = "hnguyen-cloud-resume-challenge.s3.ap-southeast-2.amazonaws.com"
            connection_attempts = 3
            connection_timeout  = 10
            origin_access_control_id = "E1YDB0U5EFCYZ9"
        }

        viewer_certificate {
            acm_certificate_arn      = "arn:aws:acm:us-east-1:464216678525:certificate/5cd2edda-aeb1-4257-8fc8-03d2ede1d92e"
            ssl_support_method       = "sni-only"
            minimum_protocol_version = "TLSv1.2_2021"
        }

        restrictions {
            geo_restriction {
            restriction_type = "none"
            }
        }

        tags = {
            project = "Cloud Resume Challenge"
        }
    }


//lambda
resource "aws_lambda_function" "lambda-resume-challenge-view-count" {
  filename         = data.archive_file.zip_the_python_code.output_path
  source_code_hash = data.archive_file.zip_the_python_code.output_base64sha256
  function_name    = "lambda-resume-challenge-view-count"
  role             = aws_iam_role.lambda_iam_resume_challenge.arn
  handler          = "lambda-resume-challenge-view-count.lambda_handler"
  runtime          = "python3.9"
}

resource "aws_iam_role" "lambda_iam_resume_challenge" {
  name = "lambda_iam_resume_challenge"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_policy" "iam_policy_for_resume_project" {

  name        = "iam_policy_for_resume_project"
  path        = "/"
  description = "AWS IAM Policy for managing the resume project role"
    policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : [
            "logs:CreateLogGroup",
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : "arn:aws:logs:*:*:*",
          "Effect" : "Allow"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "dynamodb:*"
          ],
          "Resource" : "arn:aws:dynamodb:ap-southeast-2:464216678525:table/cloud-resume-challenge-table"
        },
        {
			"Effect": "Allow",
			"Action": [
				"lambda:*"
			],
			"Resource": [
				"arn:aws:lambda:ap-southeast-2:464216678525:function:lambda-resume-challenge-view-count"
			]
		}
      ]
  })
}

resource "aws_iam_role_policy_attachment" "attach_iam_policy_to_iam_role" {
  role = aws_iam_role.lambda_iam_resume_challenge.name
  policy_arn = aws_iam_policy.iam_policy_for_resume_project.arn
}

data "archive_file" "zip_the_python_code" {
  type        = "zip"
  source_file = "${path.module}/lambda/lambda-resume-challenge-view-count.py"
  output_path = "${path.module}/packedlambda.zip"
}

resource "aws_lambda_function_url" "url1" {
  function_name      = aws_lambda_function.lambda-resume-challenge-view-count.function_name
  authorization_type = "NONE"

  cors {
    allow_credentials = true
    allow_origins     = ["https://resume.hnguyen-cloud.com"]
    allow_methods     = ["*"]
    allow_headers     = ["date", "keep-alive"]
    expose_headers    = ["keep-alive", "date"]
    max_age           = 86400
  }
}
terraform {
    required_providers {
      aws = {
        version = ">= 5.26.0"
        source  = "hashicorp/aws"
      }
    }
}

provider "aws" {
    profile = "default"
    region  = "ap-southeast-2"
}

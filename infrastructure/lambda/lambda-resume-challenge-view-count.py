import json
import boto3

dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table('cloud-resume-challenge-table')

def lambda_handler(event, context):
    try:
        response = table.get_item(Key={'id': '1'})
        if 'Item' in response:
            view = response['Item']['view']
            view += 1
            print(view)
            response = table.put_item(Item={'id': '1', 'view': view})
        else:
            view = 1
            table.put_item(Item={'id': '1', 'view': view})
    except Exception as e:
        print(e)
        raise e

    return view